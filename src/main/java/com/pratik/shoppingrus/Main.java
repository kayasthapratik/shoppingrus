package com.pratik.shoppingrus;

import com.pratik.shoppingrus.pricingRule.AppleTvThreeForTwoRule;
import com.pratik.shoppingrus.pricingRule.FreeVgaAdapterRule;
import com.pratik.shoppingrus.pricingRule.IPricingRule;
import com.pratik.shoppingrus.pricingRule.SuperIpadBulkRule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Driver class
 * */
public class Main {

    public static void main(String[] args) {

        List<IPricingRule> pricingRules = new ArrayList<>();
        pricingRules.add(new AppleTvThreeForTwoRule());
        pricingRules.add(new SuperIpadBulkRule());
        pricingRules.add(new FreeVgaAdapterRule());

        Checkout checkout = new Checkout(pricingRules);
        Arrays.stream("atv, ipd, ipd, atv, ipd, ipd, ipd".split(", ")).forEach(item -> {
            checkout.addItem(Catalogue.getItem(item));
        });

        System.out.println(checkout.calculateTotal());
    }
}
