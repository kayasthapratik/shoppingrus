package com.pratik.shoppingrus;

import java.util.HashMap;
import java.util.Map;

/**
 * Used as a look-up class for items
 * */
public class Catalogue {

    private static Map<String, Item> skuItems;
    static {
        skuItems = new HashMap<>();

        skuItems.put("ipd", new Item("ipd", 549.99, "Super iPad"));
        skuItems.put("atv", new Item("atv", 109.5, "Apple TV"));
        skuItems.put("mbp", new Item("mbp", 1399.99, "MacBook Pro"));
        skuItems.put("vga", new Item("vga", 30, "VGA adapter"));
    }

    /**
     * Returns price of item based on item sku
     * */
    public static Item getItem(String itemSku) {
        return skuItems.get(itemSku);
    }
}
