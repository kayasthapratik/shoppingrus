package com.pratik.shoppingrus;

import com.pratik.shoppingrus.pricingRule.IPricingRule;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to collect and calculate total prices (excluding discount)
 * based on pricing rules passed.
 * */
public class Checkout {

    private List<IPricingRule> pricingRules;
    private List<Item> items;

    public Checkout(List<IPricingRule> pricingRules) {
        this.pricingRules = pricingRules;
        this.items = new ArrayList<>();
    }

    public void addItem(Item item) {
        this.items.add(item);
    }

    /**
     * Calculates Net price
     * First calculates gross prices for all items,
     * then calculates total discount based on all available pricing rules
     * */
    public double calculateTotal() {
        double totalPrice = 0;
        for (Item item : this.items) {
            totalPrice = totalPrice + item.getPrice();
        }

        for (IPricingRule pricingRule : this.pricingRules) {
            totalPrice = totalPrice - pricingRule.calculateDiscount(this.items);
        }

        return totalPrice;
    }
}
