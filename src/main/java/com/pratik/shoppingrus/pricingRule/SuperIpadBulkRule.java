package com.pratik.shoppingrus.pricingRule;

import com.pratik.shoppingrus.Item;

import java.util.List;

/**
 * Super Ipad bulk discount pricing rule
 * If number of Ipad > 4, each Ipad cost only $499.99
 * */
public class SuperIpadBulkRule implements IPricingRule {

    private static final String SUPER_IPAD_SKU = "ipd";
    private static final double DISCOUNTED_PRICE = 499.99;

    @Override
    public double calculateDiscount(List<Item> items) {
        long superIpadCount = items.stream().filter(item -> item.getSku()==SUPER_IPAD_SKU).count();

        if (superIpadCount>4) {
            Item superIpadItem = items.stream()
                    .filter(item -> item.getSku()==SUPER_IPAD_SKU)
                    .findAny()
                    .get();

            return (superIpadItem.getPrice() - DISCOUNTED_PRICE) * superIpadCount;
        } else {
            return 0;
        }
    }
}
