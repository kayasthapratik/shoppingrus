package com.pratik.shoppingrus.pricingRule;

import com.pratik.shoppingrus.Item;

import java.util.List;

/**
 * EXAMPLE CLASS!
 * Class to illustrate on how to add an extra pricing rule
 * Provides $100 if all items are purchased
 * */
public class AllItemDiscountRule implements IPricingRule {

    private static final String APPLE_TV_SKU = "atv";
    private static final String MACBOOK_SKU = "mbp";
    private static final String VGA_ADAPTER_SKU = "vga";
    private static final String SUPER_IPAD_SKU = "ipd";

    @Override
    public double calculateDiscount(List<Item> items) {
        long appleTvCount = items.stream().filter(item -> item.getSku()==APPLE_TV_SKU).count();
        long macbookCount = items.stream().filter(item -> item.getSku()==MACBOOK_SKU).count();
        long vgaAdapterCount = items.stream().filter(item -> item.getSku()==VGA_ADAPTER_SKU).count();
        long superIpadCount = items.stream().filter(item -> item.getSku()==SUPER_IPAD_SKU).count();

        if (appleTvCount>0 && macbookCount>0 && vgaAdapterCount>0 && superIpadCount>0) {
            return 100;
        } else {
            return 0;
        }
    }
}
