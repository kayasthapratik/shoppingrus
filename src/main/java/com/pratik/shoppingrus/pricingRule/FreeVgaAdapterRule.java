package com.pratik.shoppingrus.pricingRule;

import com.pratik.shoppingrus.Item;

import java.util.List;

/**
 * Free VGA Adapter pricing Rule
 * Free VGA Adapter if Macbook is purchased
 * */
public class FreeVgaAdapterRule implements IPricingRule {

    private static final String MACBOOK_SKU = "mbp";
    private static final String VGA_ADAPTER_SKU = "vga";

    @Override
    public double calculateDiscount(List<Item> items) {
        long macbookCount = items.stream().filter(item -> item.getSku()==MACBOOK_SKU).count();
        long vgaAdapterCount = items.stream().filter(item -> item.getSku()==VGA_ADAPTER_SKU).count();

        if (macbookCount>0 && vgaAdapterCount>0) {
            Item vgaAdapter = items.stream()
                    .filter(item -> item.getSku()==VGA_ADAPTER_SKU)
                    .findAny()
                    .get();

            return Math.min(macbookCount, vgaAdapterCount) * vgaAdapter.getPrice();
        } else {
            return 0;
        }
    }
}
