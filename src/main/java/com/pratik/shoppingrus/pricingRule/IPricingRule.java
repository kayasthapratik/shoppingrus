package com.pratik.shoppingrus.pricingRule;

import com.pratik.shoppingrus.Item;

import java.util.List;

/**
 * Interface for Pricing Rules, each pricing rule must implement this interface.
 * If a new pricing rule is to be added, a class to be created in
 * 'com.pratik.shoppingrus.pricingRule' package that implements this interface.
 * Method 'calculateDiscount' must be overridden by the class and is to be
 * used to calculate the discount amount.
 * */
public interface IPricingRule {

    /**
     * Calculates discount amount based on items
     * */
    double calculateDiscount(List<Item> items);

}