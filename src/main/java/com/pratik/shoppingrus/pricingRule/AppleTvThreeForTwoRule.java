package com.pratik.shoppingrus.pricingRule;

import com.pratik.shoppingrus.Item;

import java.util.List;

/**
 * Pricing rule for Apple Tv's 3 for 2 rule;
 * 3 Apple Tv's for price of 2
 * */
public class AppleTvThreeForTwoRule implements IPricingRule {

    private static final String APPLE_TV_SKU = "atv";

    @Override
    public double calculateDiscount(List<Item> items) {
        long appleTvCount = items.stream().filter(item -> item.getSku()==APPLE_TV_SKU).count();

        if (appleTvCount>2) {
            Item appleTvItem = items.stream()
                                .filter(item -> item.getSku()==APPLE_TV_SKU)
                                .findAny()
                                .get();

            return Math.floorDiv(appleTvCount, 3) * appleTvItem.getPrice();
        } else {
            return 0;
        }
    }
}
