package com.pratik.shoppingrus.pricingRule;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import com.pratik.shoppingrus.Item;

public class FreeVgaAdapterRuleTest {

    private static FreeVgaAdapterRule freeVgaAdapterRule;

    @BeforeAll
    static void setupFreeVgaAdapterRule() {
        freeVgaAdapterRule = new FreeVgaAdapterRule();
    }

    @Test
    void testDiscountForFreeVgaAdapter() {
        List<Item> items = new ArrayList<>();
        items.add(new Item("mbp", 1399.99, "MacBook Pro"));

        // Test with 1 Macbook only
        assertEquals(0, freeVgaAdapterRule.calculateDiscount(items));

        // Test with 1 Macbook & vga adapter
        items.add(new Item("vga", 30, "VGA adapter"));
        assertEquals(30, freeVgaAdapterRule.calculateDiscount(items));

        // Test with 1 Macbook & 2 vga adapter
        items.add(new Item("vga", 30, "VGA adapter"));
        assertEquals(30, freeVgaAdapterRule.calculateDiscount(items));

        // Test with 2 Macbook & 2 vga adapter
        items.add(new Item("mbp", 1399.99, "MacBook Pro"));
        assertEquals(60, freeVgaAdapterRule.calculateDiscount(items));

        // Test with 3 Macbook & 2 vga adapter
        items.add(new Item("mbp", 1399.99, "MacBook Pro"));
        assertEquals(60, freeVgaAdapterRule.calculateDiscount(items));

        // Test with just 1 vga adapter
        items.clear();
        items.add(new Item("vga", 30, "VGA adapter"));
        assertEquals(0, freeVgaAdapterRule.calculateDiscount(items));
    }

    @Test
    void testDiscountWithoutVgaAndMacbook() {
        List<Item> items = new ArrayList<>();
        items.add(new Item("atv", 109.5, "Apple TV"));

        assertEquals(0, freeVgaAdapterRule.calculateDiscount(items));
    }
}
