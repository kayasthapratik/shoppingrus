package com.pratik.shoppingrus.pricingRule;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import com.pratik.shoppingrus.Item;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AllItemDiscountRuleTest {

    private static AllItemDiscountRule allItemDiscountRule;

    @BeforeAll
    static void setupAllItemDiscountRule() {
        allItemDiscountRule = new AllItemDiscountRule();
    }

    @Test
    void testAllItemDiscountRule() {
        List<Item> items = new ArrayList<>();

        // Test with apple tv only
        items.add(new Item("atv", 109.5, "Apple TV"));
        assertEquals(0, allItemDiscountRule.calculateDiscount(items));

        // Test with apple tv & Macbook
        items.add(new Item("mbp", 1399.99, "MacBook Pro"));
        assertEquals(0, allItemDiscountRule.calculateDiscount(items));

        // Test with apple tv, Macbook & VGA Adapter
        items.add(new Item("vga", 30, "VGA adapter"));
        assertEquals(0, allItemDiscountRule.calculateDiscount(items));

        // Test with apple tv, Macbook, VGA Adapter & Super Ipad
        items.add(new Item("ipd", 549.99, "Super iPad"));
        assertEquals(100, allItemDiscountRule.calculateDiscount(items));
    }
}
