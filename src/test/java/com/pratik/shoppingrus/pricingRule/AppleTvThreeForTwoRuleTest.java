package com.pratik.shoppingrus.pricingRule;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import com.pratik.shoppingrus.Item;

public class AppleTvThreeForTwoRuleTest {

    private static AppleTvThreeForTwoRule appleTvThreeForTwoRule;

    @BeforeAll
    static void setupAppleTvRule() {
        appleTvThreeForTwoRule = new AppleTvThreeForTwoRule();
    }

    @Test
    void testDiscountWithForAppleTv() {
        List<Item> items = new ArrayList<>();
        items.add(new Item("atv", 109.5, "Apple TV"));

        // Test with 1 apple TV
        assertEquals(0, appleTvThreeForTwoRule.calculateDiscount(items));

        // Test with 2 apple TVs
        items.add(new Item("atv", 109.5, "Apple TV"));
        assertEquals(0, appleTvThreeForTwoRule.calculateDiscount(items));

        // Test with 3 apple TVs
        items.add(new Item("atv", 109.5, "Apple TV"));
        assertEquals(109.5, appleTvThreeForTwoRule.calculateDiscount(items));

        // Test with 4 apple TVs
        items.add(new Item("atv", 109.5, "Apple TV"));
        assertEquals(109.5, appleTvThreeForTwoRule.calculateDiscount(items));

        // Test with 5 apple TVs
        items.add(new Item("atv", 109.5, "Apple TV"));
        assertEquals(109.5, appleTvThreeForTwoRule.calculateDiscount(items));

        // Test with 6 apple TVs
        items.add(new Item("atv", 109.5, "Apple TV"));
        assertEquals(219, appleTvThreeForTwoRule.calculateDiscount(items));
    }

    @Test
    void testDiscountWithoutAppleTv() {
        List<Item> items = new ArrayList<>();
        items.add(new Item("idp", 549.99, "Super iPad"));

        assertEquals(0, appleTvThreeForTwoRule.calculateDiscount(items));
    }
}
