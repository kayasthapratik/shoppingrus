package com.pratik.shoppingrus.pricingRule;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import com.pratik.shoppingrus.Item;

public class SuperIpadBulkRuleTest {

    private static SuperIpadBulkRule superIpadBulkRule;

    @BeforeAll
    static void setupSuperIpadRule() {
        superIpadBulkRule = new SuperIpadBulkRule();
    }

    @Test
    void testDiscountForSuperIpad() {
        List<Item> items = new ArrayList<>();
        items.add(new Item("ipd", 549.99, "Super iPad"));

        // Test with 1 super-ipad
        assertEquals(0, superIpadBulkRule.calculateDiscount(items));

        // Test with 2 super-ipad
        items.add(new Item("ipd", 549.99, "Super iPad"));
        assertEquals(0, superIpadBulkRule.calculateDiscount(items));

        // Test with 4 super-ipad
        items.add(new Item("ipd", 549.99, "Super iPad"));
        items.add(new Item("ipd", 549.99, "Super iPad"));
        assertEquals(0, superIpadBulkRule.calculateDiscount(items));

        // Test with 5 super-ipad
        items.add(new Item("ipd", 549.99, "Super iPad"));
        assertEquals(250, superIpadBulkRule.calculateDiscount(items));

        // Test with 6 super-ipad
        items.add(new Item("ipd", 549.99, "Super iPad"));
        assertEquals(300, superIpadBulkRule.calculateDiscount(items));
    }

    @Test
    void testDiscountWithoutSuperIpad() {
        List<Item> items = new ArrayList<>();
        items.add(new Item("atv", 109.5, "Apple TV"));

        assertEquals(0, superIpadBulkRule.calculateDiscount(items));
    }
}
