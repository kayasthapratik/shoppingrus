package com.pratik.shoppingrus;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import com.pratik.shoppingrus.pricingRule.AppleTvThreeForTwoRule;
import com.pratik.shoppingrus.pricingRule.FreeVgaAdapterRule;
import com.pratik.shoppingrus.pricingRule.IPricingRule;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.pratik.shoppingrus.pricingRule.SuperIpadBulkRule;

public class ShoppingRUsTest {

    private static List<IPricingRule> pricingRules;

    @BeforeAll
    static void setupPricingRules() {
        pricingRules = new ArrayList<>();

        pricingRules.add(new AppleTvThreeForTwoRule());
        pricingRules.add(new SuperIpadBulkRule());
        pricingRules.add(new FreeVgaAdapterRule());
    }

    //atv, atv, atv, vga
    @Test
    void testThreeAtvAndOneVga() {
        Checkout checkout = new Checkout(pricingRules);
        Arrays.stream("atv, atv, atv, vga".split(", ")).forEach(item -> {
            checkout.addItem(Catalogue.getItem(item));
        });

        assertEquals(249, checkout.calculateTotal());
    }

    //atv, ipd, ipd, atv, ipd, ipd, ipd
    @Test
    void testFiveIptAndTwoAtv() {
        Checkout checkout = new Checkout(pricingRules);
        Arrays.stream("atv, ipd, ipd, atv, ipd, ipd, ipd".split(", ")).forEach(item -> {
            checkout.addItem(Catalogue.getItem(item));
        });

        assertEquals(2718.95, checkout.calculateTotal());
    }

    //mbp, vga, ipd
    @Test
    void testMbpVgaAndIpd() {
        Checkout checkout = new Checkout(pricingRules);
        Arrays.stream("mbp, vga, ipd".split(", ")).forEach(item -> {
            checkout.addItem(Catalogue.getItem(item));
        });

        assertEquals(1949.98, checkout.calculateTotal());
    }
}
