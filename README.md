# Shopping R Us

Simple java project to calculate net prices of items based on different pricing rules.

## Contents
The pricing rules are located at com.pratik.shoppingrus.pricingRule, each pricing rule is represented as a class that implements IPricingRule interface. Each of these class is responsible for calculating total discount based on number of items in the 'items' list.

To add an extra pricing rule, a new class needs to be created that implements IPricingRule interface and override 'calculateDiscount' method to return discount amount.

'Checkout' class is used as a container for all items and pricing rules, it represents a transaction. A list of pricing rules is to be provided to this class to calculate net price.

## How to run
This is a simple gradle project, it has a driver class called 'Main', it can be run by issuing following command in command prompt.

    ./gradlew run

You can run the tests by issuing the following command

    ./gradlew test

## Requirements
- JDK1.8
- Gradle